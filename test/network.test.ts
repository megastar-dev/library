// Create contract data for specified hedge units, start price and protection
import test, { ExecutionContext } from 'ava';
import { ContractData } from '../lib/interfaces';
import AnyHedgeManager from '../lib/anyhedge';
import { START_BLOCK, ORACLE_PUBKEY, HEDGE_PUBKEY, LONG_PUBKEY, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_LIQUIDATION_HEIGHT, DUMMY_HASH, ORACLE_WIF } from './fixture/constants';

const { OracleData } = require('@generalprotocols/price-oracle');

// Uses constant fixture values for all other parameters of manager.create()
const loadContractData = async function(
	manager: AnyHedgeManager,
	hedgeUnits: number,
	startPrice: number,
): Promise<ContractData>
{
	// Allow for immediate settlement and disallow maturity for the foreseeable future
	const earliestLiquidationModifier = 0;
	const maturityModifier = 1000000;

	const lowLiquidationPriceMultiplier = 1 - DEFAULT_VOLATILITY_PROTECTION;

	const contractData = await manager.create(
		ORACLE_PUBKEY,
		HEDGE_PUBKEY,
		LONG_PUBKEY,
		hedgeUnits,
		startPrice,
		START_BLOCK,
		earliestLiquidationModifier,
		maturityModifier,
		DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier,
	);

	return contractData;
};

// This test can be used to check network functionality on mainnet.
// It is currently set up to burn money, but we could update it so that it
// doesn't cost money (because we receive the money back from the contract)
// Usage right now:
// 1. Remove '.skip' from the test.
// 2. Run `npm run build`
// 3. Run `./node_modules/.bin/ava test/network.test.ts --verbose` (Will fail)
// 4. Send around 10k satoshis the logged address
// 5. Run (3) again (Will succeed) - check logged txid
test.skip('should send transaction to the network', async (t: ExecutionContext) =>
{
	// Set up anyhedge manager
	const manager = new AnyHedgeManager();
	manager.load();

	// Load contract data
	const contractData = await loadContractData(manager, 1, 20000);

	// Retrieve and log contract address
	const contractInstance = await manager.build(contractData.hashes);
	t.log(contractInstance.address);

	// Set up test oracle data
	const oracleMessage = await OracleData.createPriceMessage(10000, DEFAULT_LIQUIDATION_HEIGHT, DUMMY_HASH, 1, 1, 1);
	const oracleSignature = await OracleData.signMessage(oracleMessage, ORACLE_WIF);

	// Liquidate contract
	const txid = await manager.liquidate(
		ORACLE_PUBKEY,
		oracleMessage,
		oracleSignature,
		HEDGE_PUBKEY,
		LONG_PUBKEY,
		contractData.metadata,
		contractData.parameters,
		contractData.hashes,
	);

	// Log txid
	t.log(txid);

	t.pass();
});
