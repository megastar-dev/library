export interface Usecase
{
	contract: ContractFixture;
	liquidation: LiquidationFixture;
	maturation: MaturationFixture;
}

export interface ContractFixture
{
	register: MethodFixture;
	create: MethodFixture;
	validate: MethodFixture;
}

export interface LiquidationFixture
{
	simulate: MethodFixture;
}

export interface MaturationFixture
{
	simulate: MethodFixture;
}

export interface MethodFixture
{
	input: any[];
	output: any;
}
