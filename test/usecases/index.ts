import { Usecase } from './interfaces';
import hedge10week from './hedge10week';

export { Usecase } from './interfaces';
export const usecases: { [index: string]: Usecase } =
{
	// Hedge 10 USD for one week.
	hedge10week,
};
