const test = require('tape');

const AnyHedge = require('../lib/anyhedge.js');

test('anyHedgeContract.untrunc() normal cases', function (t)
{
	const contract = new AnyHedge();

	// bytes = 0 to 4
	for (let i = 0, j = 1; i <= 4; i += 1, j *= 256)
	{
		t.equal(contract.untrunc(1, i), j, `bytes=${i}`);
	}

	// different data in the multiple of 9, so that it lies with the 53bit safe integer range
	for (let i = 0, j = 1; i < 10; i += 1, j *= 9)
	{
		t.equal(contract.untrunc(j, 2), j * 65536, `data=${j}`);
	}

	t.end();
});

test('anyHedgeContract.untrunc() boundary cases', function (t)
{
	const contract = new AnyHedge();

	// check the boundaries value of bytes -1 and 5
	t.equal(contract.untrunc(1, -1), Math.floor(0.00390625), 'bytes=-1');
	t.equal(contract.untrunc(1, 5), 1099511627776, 'bytes=5');

	t.end();
});

test('anyHedgeContract.untrunc() other integral cases', function (t)
{
	const contract = new AnyHedge();

	// different data in the multiple of 2^32
	for (let i = 0, j = 1; i < 10; i += 1, j *= 4294967296)
	{
		t.equal(contract.untrunc(j, 2), j * 65536, `data=${j}`);
	}

	t.equal(contract.untrunc(0, 2), 0, 'data=0');

	// other values of bytes
	t.equal(contract.untrunc(2, 126), 2 * Math.pow(2, 1008), 'a return value close to maximum integral value');
	t.equal(contract.untrunc(2, -126), Math.floor(2 * Math.pow(2, -1008)), 'a return value close to minimum integral value');

	t.end();
});

test('anyHedgeContract.untrunc() edge cases', function (t)
{
	const contract = new AnyHedge();

	// Max value in Javascript is 2^1024 - 1
	t.equal(contract.untrunc(1 * Math.pow(2, 1016), 1), Infinity, 'a return value larger than maximum integral');

	// floating-point bytes is not an expected input but valid value
	t.equal(contract.untrunc(0.1, 1), Math.floor(25.6), 'Undetermined case with Math.pow() behaviour with floating point');

	// negative value is not an expected input but valid value
	t.equal(contract.untrunc(-256, 127), -Infinity, 'Undetermined case with Math.pow() behaviour with negative value');

	// string type
	t.equal(contract.untrunc('256', '1'), 0x10000, 'data and bytes in string type');

	// undefined value
	t.ok(isNaN(contract.untrunc(256, undefined)), 'bytes=undefined');
	t.ok(isNaN(contract.untrunc(undefined, 2)), 'data=undefined');

	// null value: They are undetermined value because of Javascript
	t.equal(contract.untrunc(256, null), 256, 'bytes=null');
	t.equal(contract.untrunc(null, 2), 0, 'data=null');

	t.end();
});
