const test = require('tape');

const AnyHedge = require('../lib/anyhedge.js');

test('anyHedgeContract.trunc() normal cases', function (t)
{
	const contract = new AnyHedge();

	// bytes = 0 to 4
	for (let i = 0, j = 4 * 256; i <= 4; i += 1, j /= 256)
	{
		t.equal(contract.trunc(4 * 256, i), Math.floor(j), `bytes=${i}`);
	}

	// different data in the multiple of 2^32
	for (let i = 0, j = 1; i < 10; i += 1, j *= 9)
	{
		t.equal(contract.trunc(j, 2), Math.floor(j / 65536), `data=${j}`);
	}

	t.end();
});

test('anyHedgeContract.trunc() boundary cases', function (t)
{
	const contract = new AnyHedge();

	// check the boundaries value of bytes -1 and 5
	t.equal(contract.trunc(1, -1), 256, 'bytes=-1');
	t.equal(contract.trunc(0X10000000000, 5), 1, 'bytes=5');

	t.end();
});

test('anyHedgeContract.trunc() other integral cases', function (t)
{
	const contract = new AnyHedge();

	// different data in the multiple of 2^32
	for (let i = 0, j = 1; i < 10; i += 1, j *= 4294967296)
	{
		t.equal(contract.trunc(j, 2), Math.floor(j / 65536), `data=${j}`);
	}

	t.equal(contract.trunc(0x123000, 1), 0x1230);

	t.equal(contract.trunc(0, 1), 0, 'data=0');

	t.equal(contract.trunc(2 * Math.pow(2, 984), 123), 2, 'a dividend value and divisor value close to maximum integral value');
	t.equal(contract.trunc(0x1230 * Math.pow(2, 800), 100), 0x1230, 'a dividend value and divisor value close to maximum integral value');

	t.end();
});

test('anyHedgeContract.trunc() invalid cases', function (t)
{
	const contract = new AnyHedge();

	t.equal(contract.trunc(-1, 128), -0, 'Divided by infinity');

	// floating-point bytes is not an expected input but valid value
	t.equal(contract.trunc(0.1, 1), Math.floor(0.000390625), 'Undetermined case with Math.pow() behaviour with floating point');

	// negative value is not an expected input but valid value
	t.equal(contract.trunc(-100, 1), Math.floor(-0.390625), 'Undetermined case with Math.pow() behaviour with negative data value');

	t.equal(contract.trunc('256', '1'), 1, 'data and bytes in string type');

	// undefined value
	t.ok(isNaN(contract.trunc(256, undefined)), 'bytes=undefined');
	t.ok(isNaN(contract.trunc(undefined, 2)), 'data=undefined');

	// null value: They are undetermined value because of Javascript
	t.equal(contract.trunc(256, null), 256, 'bytes=null');
	t.equal(contract.trunc(null, 2), 0, 'data=null');

	t.end();
});
