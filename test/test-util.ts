import { Transaction, Utxo } from 'cashscript';
import { AuthenticationProgramBCH, instantiateVirtualMachineBCH, Output, hexToBin, binToHex, encodeTransaction, decodeTransactionUnsafe, bigIntToBinUint64LE, binToBigIntUint64LE } from '@bitauth/libauth';
import AnyHedgeManager from '../lib/anyhedge';
import { ContractData } from '../lib/interfaces';
import { addressToLockScript } from '../lib/util/bitcoincash-util';

// Create a fake version of the contract.broadcast() function that sets the transaction's
// locktime to a specific value and returns the built transaction
export type BroadcastFunction = (transactionBuilder: Transaction) => Promise<string>;
export const createFakeBroadcastWithFakeLocktime = function(locktime: number): BroadcastFunction
{
	const fakeBroadcast =
		async (transactionBuilder: Transaction): Promise<string> => transactionBuilder.withTime(locktime).build();

	return fakeBroadcast;
};

// Create a fake version of the contract.findContractCoins() function that returns
// a specific set of (fake) inputs
export type FindContractCoinsFunction = () => Promise<Utxo[]>;
export const createFakeFindContractCoins = function(inputs: Utxo[]): FindContractCoinsFunction
{
	const fakeFindContractCoins = async (): Promise<Utxo[]> => inputs;

	return fakeFindContractCoins;
};

// Create a libauth compatible source output to use in the Authentication Program
export const createSourceOutput = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	satoshis: number,
): Promise<Output>
{
	const contractInstance = await manager.build(contractData.hashes);
	const lockingBytecode = addressToLockScript(contractInstance.address);
	const sourceOutput = { lockingBytecode, satoshis: bigIntToBinUint64LE(BigInt(satoshis)) };

	return sourceOutput;
};

// Create a libauth compatible Authentication Program to be evaluated
// !! Assumes that only a single transaction input is used
export const createProgram = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	transactionHex: string,
	satoshis: number,
): Promise<AuthenticationProgramBCH>
{
	const inputIndex = 0;
	const sourceOutput = await createSourceOutput(manager, contractData, satoshis);
	// Note: we use decodeTransactionUnsafe() because we know the input transaction hex is valid
	const spendingTransaction = decodeTransactionUnsafe(hexToBin(transactionHex));
	const program = { inputIndex, sourceOutput, spendingTransaction };

	return program;
};

// Evaluate a libauth Authentication Program on a libauth VM
export const evaluateProgram = async function(program: AuthenticationProgramBCH): Promise<string | true>
{
	const vm = await instantiateVirtualMachineBCH();
	const finalState = vm.evaluate(program);
	const result = vm.verify(finalState);

	return result;
};

// Generates the meep command to debug a libauth Authentication Program
export const meepProgram = function(program: AuthenticationProgramBCH): string
{
	// Serialise the spending transaction from a Transaction object to a hex string
	const transactionHex = binToHex(encodeTransaction(program.spendingTransaction));

	// Extract the input index from the program
	const { inputIndex } = program;

	// Serialise the UTXO amount from a 64-bit little endian Uint8Array to a number
	const inputAmount = binToBigIntUint64LE(program.sourceOutput.satoshis);

	// Serialise the locking script from a Uint8Array to a hex string
	const inputLockingScript = binToHex(program.sourceOutput.lockingBytecode);

	return `meep debug --tx=${transactionHex} --idx=${inputIndex} --amt=${inputAmount} --pkscript=${inputLockingScript}`;
};
