module.exports =
{
	extends: ['@generalprotocols/eslint-config/typescript'],
	parserOptions:
	{
		project: './tsconfig.json',
		ecmaVersion: 2018,
		sourceType: 'module'
	},
}
