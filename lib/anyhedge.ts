import { BITBOX } from 'bitbox-sdk';
import { Contract, SignatureTemplate, Transaction, Utxo, Instance } from 'cashscript';
import fetch from 'node-fetch';
import { DUST_LIMIT, SATS_PER_BCH, MIN_TRANSACTION_FEE, DEFAULT_SERVICE_DOMAIN, DEFAULT_SERVICE_PORT, SCRIPT_INT_MAX, MAX_HEDGE_UNITS, MIN_HEDGE_UNITS, MAX_START_PRICE, MIN_START_PRICE, MAX_HIGH_LIQUIDATION_PRICE, MIN_LOW_LIQUIDATION_PRICE } from './constants';
import { ContractHashes, ContractMetadata, ContractParameters, SimulationOutput, ContractData } from './interfaces';
import { debug, bufferReviver, isInt } from './util/javascript-util';
import { untruncScriptNum, calculateRequiredScriptNumTruncation, truncScriptNum, calculateTotalSats, dustsafe, scriptNumSize } from './util/anyhedge-util';
import { buildLockScriptP2PKH, addressToElectrumScriptHash } from './util/bitcoincash-util';
import electrum from './util/electrum';

const { OracleData } = require('@generalprotocols/price-oracle');

// Initialize the BITBOX Bitcoin Cash library
const bitbox = new BITBOX();

/**
 * Class that manages AnyHedge contract operations, such as creation, validation, and payout of AnyHedge contracts.
 */
class AnyHedgeManager
{
	contractFactory: Contract;

	/**
	 * Loads a contract from file and prepares a contract factory for it.
	 *
	 * @param [contractFile]   Optional filepath for an alternative contract file to use instead of the default.
	 */
	async load(contractFile?: string): Promise<void>
	{
		try
		{
			// Use default contract if none was specified.
			const filename = (contractFile || require.resolve('./anyhedge.cash'));

			// Compile the contract.
			this.contractFactory = Contract.compile(filename, 'mainnet');
		}
		catch(error)
		{
			debug.errors('Failed to compile contract.');
			debug.object(error);
		}
	}

	/*
	// External library functions
	*/

	/**
	 * Register a new contract for external management.
	 *
	 * @param oraclePublicKeyBuffer           public key for the oracle that the contract trusts for price messages.
	 * @param hedgePublicKeyBuffer            public key for the hedge party.
	 * @param longPublicKeyBuffer             public key for the long party.
	 * @param hedgeUnits                      amount in units that the hedge party wants to protect against volatility.
	 * @param startPrice                      starting price (units/BCH) of the contract.
	 * @param startBlockHeight                blockHeight at which the contract is considered to have been started at.
	 * @param earliestLiquidationModifier     minimum number of blocks from the starting height before the contract can be liquidated.
	 * @param maturityModifier                exact number of blocks from the starting height that the contract should mature at.
	 * @param highLiquidationPriceMultiplier  multiplier for the startPrice determining the upper liquidation price boundary.
	 * @param lowLiquidationPriceMultiplier   multiplier for the startPrice determining the lower liquidation price boundary.
	 * @param [serviceDomain]                 fully qualified domain name for the settlement service provider to register with.
	 * @param [servicePort]                   network port number for the settlement service provider to register with.
	 *
	 * @returns contract information for the registered contract.
	 */
	async register(
		oraclePublicKeyBuffer: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		hedgeUnits: number,
		startPrice: number,
		startBlockHeight: number,
		earliestLiquidationModifier: number,
		maturityModifier: number,
		highLiquidationPriceMultiplier: number,
		lowLiquidationPriceMultiplier: number,
		serviceDomain = DEFAULT_SERVICE_DOMAIN,
		servicePort = DEFAULT_SERVICE_PORT,
	): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'register() <=', arguments ]);

		const contractCreationParameters =
		{
			// Public keys
			oraclePublicKey: oraclePublicKeyBuffer.toString('hex'),
			hedgePublicKey: hedgePublicKeyBuffer.toString('hex'),
			longPublicKey: longPublicKeyBuffer.toString('hex'),

			// Contract parameters
			hedgeUnits: hedgeUnits,
			startPrice: startPrice,
			startBlockHeight: startBlockHeight,
			earliestLiquidationModifier: earliestLiquidationModifier,
			maturityModifier: maturityModifier,
			highLiquidationPriceMultiplier: highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier: lowLiquidationPriceMultiplier,
		};

		// Register the contract with the automatic settlement service provider.
		const requestParameters =
		{
			method: 'post',
			body: JSON.stringify(contractCreationParameters),
			headers: { 'Content-Type': 'application/json' },
		};
		const contractResponse = await fetch(`http://${serviceDomain}:${servicePort}/contract`, requestParameters);

		// If the API call was not successful..
		if(!contractResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await contractResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const contractData = await JSON.parse(await contractResponse.json(), bufferReviver);

		// Write log entry for easier debugging.
		debug.action(`Registered a newly created contract with ${serviceDomain}:${servicePort} for automatic settlement.`);

		// Validate that the contract returned is identical to a contract created locally.
		const contractValidity = await this.validate(
			contractData.contract.address,
			oraclePublicKeyBuffer,
			hedgePublicKeyBuffer,
			longPublicKeyBuffer,
			hedgeUnits,
			startPrice,
			startBlockHeight,
			earliestLiquidationModifier,
			maturityModifier,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
		);

		// If the contract is invalid..
		if(!contractValidity)
		{
			// Write a log entry explaining the problem and throw the error.
			const errorMsg = `Contract registration with ${serviceDomain}:${servicePort} resulted in an invalid contract.`;
			debug.errors(errorMsg);

			throw(new Error(errorMsg));
		}

		// Output function result for easier collection of test data.
		debug.result([ 'register() =>', contractData ]);

		// Return the contract information.
		return contractData;
	}

	/**
	 * Validates that a given contract address matches specific contract parameters.
	 *
	 * @param contractAddress                   contract address encoded according to the cashaddr specification.
	 * @param oraclePublicKeyBuffer             public key for the oracle that the contract trusts for price messages.
	 * @param hedgePublicKeyBuffer              public key for the hedge party.
	 * @param longPublicKeyBuffer               public key for the long party.
	 * @param hedgeUnits                        amount in units that the hedge party wants to protect against volatility.
	 * @param startPrice                        starting price (units/BCH) of the contract.
	 * @param startBlockHeight                  blockHeight at which the contract is considered to have been started at.
	 * @param earliestLiquidationModifier       minimum number of blocks from the starting height before the contract can be liquidated.
	 * @param maturityModifier                  exact number of blocks from the starting height that the contract should mature at.
	 * @param highLiquidationPriceMultiplier    multiplier for the startPrice determining the upper liquidation price boundary.
	 * @param lowLiquidationPriceMultiplier     multiplier for the startPrice determining the lower liquidation price boundary.
	 *
	 * @returns true if the contract address and parameters match, otherwise false.
	 */
	async validate(
		contractAddress: string,
		oraclePublicKeyBuffer: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		hedgeUnits: number,
		startPrice: number,
		startBlockHeight: number,
		earliestLiquidationModifier: number,
		maturityModifier: number,
		highLiquidationPriceMultiplier: number,
		lowLiquidationPriceMultiplier: number,
	): Promise<boolean>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'validate() <=', arguments ]);

		// Prepare the contract.
		const contractData = await this.create(
			oraclePublicKeyBuffer,
			hedgePublicKeyBuffer,
			longPublicKeyBuffer,
			hedgeUnits,
			startPrice,
			startBlockHeight,
			earliestLiquidationModifier,
			maturityModifier,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
		);

		// Build the contract.
		const contractInstance = await this.build(contractData.hashes);

		// Calculate contract validity.
		const contractValidity = (contractAddress === contractInstance.address);

		if(contractValidity)
		{
			// Write log entry for easier debugging.
			debug.action('Validated a contract address against provided contract parameters.');
			// eslint-disable-next-line prefer-rest-params
			debug.object(arguments);
		}
		else
		{
			// Write log entry for easier debugging.
			debug.errors(`Failed to validate the provided contract address (${contractAddress}) against provided contract parameters generated address (${contractInstance.address}).`);
			// eslint-disable-next-line prefer-rest-params
			debug.object(arguments);
		}

		// Output function result for easier collection of test data.
		debug.result([ 'validate() =>', contractValidity ]);

		// Return the validity of the contract.
		return contractValidity;
	}

	/**
	* Fetches the total balance on a contract.
	*
	* @param contractHashes   contract hashes required to build the contract instance.
	*
	* @returns the total number of unspent satoshis at the contracts address.
	*/
	async balance(contractHashes: ContractHashes): Promise<number>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'balance() <=', arguments ]);

		// Build the contract instance.
		const contractInstance = await this.build(contractHashes);

		const balance = await contractInstance.getBalance();

		// Write log entry for easier debugging.
		debug.action('Fetched available on-chain balance for contract.');

		// Output function result for easier collection of test data.
		debug.result([ 'balance() =>', balance ]);

		// Return the balance of the contract.
		return balance;
	}

	/**
	 * Mutual cancellation based on original amounts.
	 *
	 * @param hedgePrivateKeyWIF   private key of the hedge party.
	 * @param longPrivateKeyWIF    private key of the long party.
	 * @param contractMetadata     contract metadata with hedge and longs input satoshis.
	 * @param contractHashes       contract hashes required to build the contract instance.
	 *
	 * @returns the TXID of a successful cancellation.
	 */
	async abort(
		hedgePrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		contractMetadata: ContractMetadata,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'abort() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Attempting to abort a contract.');

		// Cancel the contract with the original amounts.
		const cancellationResult = await this.cancel(
			hedgePrivateKeyWIF,
			longPrivateKeyWIF,
			contractMetadata.hedgeInputSats,
			contractMetadata.longInputSats,
			contractHashes,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'abort() =>', cancellationResult ]);

		// Return the cancellation results.
		return cancellationResult;
	}

	/**
	 * Mutual cancellation based on given valuation.
	 *
	 * This is practically a mutually agreed upon maturation prior to the maturity height.
	 *
	 * @param hedgePrivateKeyWIF   private key of the hedge party.
	 * @param longPrivateKeyWIF    private key of the long party.
	 * @param settlementPrice      price (units/BCH) to use for the settlement.
	 * @param contractParameters   contract parameters necessary to simulate maturity outcome.
	 * @param contractHashes       contract hashes required to build the contract instance.
	 *
	 * @returns the TXID of a successful cancellation.
	 */
	async settle(
		hedgePrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		settlementPrice: number,
		contractParameters: ContractParameters,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'settle() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Attempting to settle a contract.');

		// Fetch the current contract balance.
		const totalSats = await this.balance(contractHashes);

		// Calculate outcome amounts based on valuation.
		const outcome = await this.simulate(contractParameters, totalSats, settlementPrice);

		// Cancel the contract with the calculated outcome.
		const cancellationResult = await this.cancel(
			hedgePrivateKeyWIF,
			longPrivateKeyWIF,
			outcome.hedgePayoutSats,
			outcome.longPayoutSats,
			contractHashes,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'settle() =>', cancellationResult ]);

		// Return the settlement results.
		return cancellationResult;
	}

	/**
	 * Mutual cancellation of a contract with arbitrary numbers.
	 *
	 * @param hedgePrivateKeyWIF   private key of the hedge party.
	 * @param longPrivateKeyWIF    private key of the long party.
	 * @param hedgePayoutSats      number of satoshis to pay to the hedge party.
	 * @param longPayoutSats       number of satoshis to pay to the long party.
	 * @param contractHashes       contract hashes required to build the contract instance.
	 *
	 * @returns the TXID of a successful cancellation.
	 */
	async cancel(
		hedgePrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		hedgePayoutSats: number,
		longPayoutSats: number,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'cancel() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Cancelling a contract.');

		try
		{
			// Derive hedge and long keypairs.
			const hedgePrivateKeyPair = bitbox.ECPair.fromWIF(hedgePrivateKeyWIF);
			const longPrivateKeyPair = bitbox.ECPair.fromWIF(longPrivateKeyWIF);

			// Derive hedge and long public keys.
			const hedgePublicKeyBuffer = bitbox.ECPair.toPublicKey(hedgePrivateKeyPair);
			const longPublicKeyBuffer = bitbox.ECPair.toPublicKey(longPrivateKeyPair);

			// Create hedge and long signature templates
			const hedgeSigTemplate = new SignatureTemplate(hedgePrivateKeyPair);
			const longSigTemplate = new SignatureTemplate(longPrivateKeyPair);

			// Build the contract instance.
			const contractInstance = await this.build(contractHashes);

			// Fetch all UTXOs at the address.
			const coins = await this.findContractCoins(contractInstance);

			// Derive hedge and long address.
			const hedgeAddress = bitbox.ECPair.toCashAddress(hedgePrivateKeyPair);
			const longAddress = bitbox.ECPair.toCashAddress(longPrivateKeyPair);

			// Create a cancellation transaction..
			const cancellationTransaction = contractInstance.functions
				.mutualRedeem(hedgePublicKeyBuffer, longPublicKeyBuffer, hedgeSigTemplate, longSigTemplate)
				.from(coins)
				.withMinChange(Number.MAX_VALUE)
				.to(hedgeAddress, hedgePayoutSats)
				.to(longAddress, longPayoutSats);

			// Broadcast the transaction.
			const broadcastResult = await this.broadcast(cancellationTransaction);

			// Output function result for easier collection of test data.
			debug.result([ 'cancel() =>', broadcastResult ]);

			// Return the broadcast result.
			return broadcastResult;
		}
		catch(error)
		{
			// Log an error message.
			debug.errors('Failed to cancel contract: ', error);

			// Throw the error.
			throw(new Error(`Failed to cancel contract: ${error}`));
		}
	}

	/**
	 * Liquidates a contract.
	 *
	 * @param oraclePublicKey        public key of the oracle that provided the price message.
	 * @param oracleMessage          price message to use for liquidation.
	 * @param oracleSignature        signature for the price message.
	 * @param hedgePublicKeyBuffer   public key of the hedge party.
	 * @param longPublicKeyBuffer    public key of the long party.
	 * @param contractMetadata       contract metadata required to determine available satoshis.
	 * @param contractParameters     contract parameters required to simulate liquidation outcome.
	 * @param contractHashes         contract hashes required to build the contract instance.
	 *
	 * @returns the TXID of a successful liquidation.
	 */
	async liquidate(
		oraclePublicKey: Buffer,
		oracleMessage: Buffer,
		oracleSignature: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		contractMetadata: ContractMetadata,
		contractParameters: ContractParameters,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'liquidate() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Attempting to liquidate a contract.');

		// Parse the oracle message.
		const oracleData = await OracleData.parsePriceMessage(oracleMessage);

		// Validate that the oracles message blockheight is equal or higher than the contracts earliest liquidate height.
		if(oracleData.blockHeight < contractParameters.earliestLiquidationHeight)
		{
			// Log an error message.
			debug.errors('Cannot liquidate a contract before its earliest liquidation height.');

			// Throw error.
			throw(new Error('Cannot liquidate a contract before its earliest liquidation height.'));
		}

		// Validate that the oracles message blockheight is lower than the maturation height.
		if(oracleData.blockHeight >= contractParameters.maturityHeight)
		{
			// Log an error message.
			debug.errors('Cannot liquidate a contract at or after its maturation height.');

			// Throw error.
			throw(new Error('Cannot liquidate a contract at or after its maturation height.'));
		}

		// Validate that the oracles price is strictly inside liquidation boundaries.
		if(oracleData.price > contractParameters.lowLiquidationPrice && oracleData.price < contractParameters.highLiquidationPrice)
		{
			// Log an error message.
			debug.errors('Cannot liquidate a contract at a price within the contract boundaries.');

			// Throw the error.
			throw(new Error('Cannot liquidate a contract at a price within the contract boundaries.'));
		}

		// Validate that the oracles price is not zero or negative.
		if(oracleData.price <= 0)
		{
			// Log an error message.
			debug.errors('Cannot liquidate a contract at a price of <= 0.');

			// Throw the error.
			throw(new Error('Cannot liquidate a contract at a price of <= 0.'));
		}

		// Calculate contract outcomes.
		const totalSats = calculateTotalSats(contractMetadata);
		const outcome = await this.simulate(contractParameters, totalSats, oracleData.price);

		// Redeem the contract.
		const liquidationResult = await this.payout(
			oraclePublicKey,
			oracleMessage,
			oracleSignature,
			hedgePublicKeyBuffer,
			longPublicKeyBuffer,
			outcome.hedgePayoutSats,
			outcome.longPayoutSats,
			contractParameters,
			contractHashes,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'liquidate() =>', liquidationResult ]);

		// Return the liquidation result.
		return liquidationResult;
	}

	/**
	 * Matures a contract.
	 *
	 * @param oraclePublicKey        public key of the oracle that provided the price message.
	 * @param oracleMessage          price message to use for maturation.
	 * @param oracleSignature        signature for the price message.
	 * @param hedgePublicKeyBuffer   public key of the hedge party.
	 * @param longPublicKeyBuffer    public key of the long party.
	 * @param contractMetadata       contract metadata required to determine available satoshis.
	 * @param contractParameters     contract parameters required to simulate maturation outcome.
	 * @param contractHashes         contract hashes required to build the contract instance.
	 *
	 * @returns the TXID of a successful maturation.
	 */
	async mature(
		oraclePublicKey: Buffer,
		oracleMessage: Buffer,
		oracleSignature: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		contractMetadata: ContractMetadata,
		contractParameters: ContractParameters,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'mature() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Attempting to mature a contract.');

		// Parse the oracle message.
		const oracleData = await OracleData.parsePriceMessage(oracleMessage);

		// Validate that the oracle messages blockheight is equal to the contracts maturity height.
		if(oracleData.blockHeight !== contractParameters.maturityHeight)
		{
			// Log an error message.
			debug.errors('Cannot mature a contract before its maturity height.');

			// Throw an error.
			throw(new Error('Cannot mature a contract before its maturity height.'));
		}

		// NOTE: We let the miners validate that the current blockheight is after the maturity height, to avoid network lookups.

		// Validate the that oracle messages block sequence number is 1.
		if(oracleData.blockSequence !== 1)
		{
			// Log an error message.
			debug.errors('Cannot mature a contract with an oracle block sequence other than 1.');

			// Throw an error.
			throw(new Error('Cannot mature a contract with an oracle block sequence other than 1.'));
		}

		// Validate that the oracles price is not zero or negative.
		if(oracleData.price <= 0)
		{
			// Log an error message.
			debug.errors('Cannot liquidate a contract at a price of <= 0.');

			// Throw the error.
			throw(new Error('Cannot liquidate a contract at a price of <= 0.'));
		}

		// Calculate contract outcomes.
		const totalSats = calculateTotalSats(contractMetadata);
		const outcome = await this.simulate(contractParameters, totalSats, oracleData.price);

		// Redeem the contract.
		const maturationResult = await this.payout(
			oraclePublicKey,
			oracleMessage,
			oracleSignature,
			hedgePublicKeyBuffer,
			longPublicKeyBuffer,
			outcome.hedgePayoutSats,
			outcome.longPayoutSats,
			contractParameters,
			contractHashes,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'mature() =>', maturationResult ]);

		// Return the liquidation result.
		return maturationResult;
	}

	/**
	 * Redeems the contract with arbitrary numbers.
	 *
	 * @param oraclePublicKey        public key of the oracle that provided the price message.
	 * @param oracleMessage          price message to use for payout.
	 * @param oracleSignature        signature for the price message.
	 * @param hedgePublicKeyBuffer   public key of the hedge party.
	 * @param longPublicKeyBuffer    public key of the long party.
	 * @param hedgePayoutSats        number of satoshis to pay out to the hedge party.
	 * @param longPayoutSats         number of satoshis to pay out to the long party.
	 * @param contractParameters     contract parameters required to unlock the redemption function.
	 * @param contractHashes         contract hashes required to build the contract instance.
	 *
	 * @returns the TXID of a successful redemption.
	 */
	async payout(
		oraclePublicKey: Buffer,
		oracleMessage: Buffer,
		oracleSignature: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		hedgePayoutSats: number,
		longPayoutSats: number,
		contractParameters: ContractParameters,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'payout() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Paying out a contract.');

		try
		{
			// Build the contract instance.
			const contractInstance = await this.build(contractHashes);

			// Fetch the coins on the contract.
			const coins = await this.findContractCoins(contractInstance);

			// Generate a new random redeemwallet.
			const redeemWallet = bitbox.HDNode.fromSeed(bitbox.Mnemonic.toSeed(bitbox.Mnemonic.generate(128)));

			// Store the redeemwallet's public key as the preimage pubkey.
			const preimagePubkey = redeemWallet.getPublicKeyBuffer();
			const preimageSigTemplate = new SignatureTemplate(redeemWallet.keyPair);

			// Derive the hedge and long address.
			const hedgePublicAddress = bitbox.ECPair.toCashAddress(bitbox.ECPair.fromPublicKey(hedgePublicKeyBuffer));
			const longPublicAddress = bitbox.ECPair.toCashAddress(bitbox.ECPair.fromPublicKey(longPublicKeyBuffer));

			// Create the transaction.
			// NOTE: CashScript automatically sets nSequence to allow for absolute locktimes
			// and sets nLocktime to the most recent block number
			const payoutTransaction = contractInstance.functions
				.payout(
					// // FIXED // //
					// Outputs
					contractParameters.hedgeLockScript,
					contractParameters.longLockScript,
					// Oracle
					contractParameters.oraclePubk,
					// Money
					contractParameters.hedgeUnitsXSatsPerBchHighTrunc,
					contractParameters.highLowDeltaTruncatedZeroes,
					contractParameters.payoutSatsLowTrunc,
					contractParameters.lowTruncatedZeroes,
					contractParameters.lowLiquidationPrice,
					contractParameters.highLiquidationPrice,
					// Time
					contractParameters.earliestLiquidationHeight,
					contractParameters.maturityHeight,
					// // REDEMPTION // //
					// Transaction verification
					preimageSigTemplate,
					preimagePubkey,
					// Oracle data
					oracleMessage,
					oracleSignature,
				)
				.from(coins)
				.withMinChange(Number.MAX_VALUE)
				.to(hedgePublicAddress, hedgePayoutSats)
				.to(longPublicAddress, longPayoutSats);

			// Broadcast the transaction.
			const broadcastResult = await this.broadcast(payoutTransaction);

			// Output function result for easier collection of test data.
			debug.result([ 'payout() =>', broadcastResult ]);

			// Return the broadcast result.
			return broadcastResult;
		}
		catch(error)
		{
			// Log an error message.
			debug.errors('Failed to payout contract: ', error);

			// Throw the error.
			throw(new Error(`Failed to payout contract: ${error}`));
		}
	}

	/*
	// Internal library functions
	*/

	/**
	 * Wrapper that broadcasts a prepared transaction using the CashScript SDK.
	 *
	 * @param transactionBuilder   fully prepared transaction builder ready to execute its broadcast() function.
	 *
	 * @returns the TXID of a successful transaction.
	 *
	 * @ignore
	 */
	async broadcast(transactionBuilder: Transaction): Promise<string>
	{
		// Build raw transaction hex
		const rawTransactionHex = await transactionBuilder.build();

		try
		{
			// Wait for electrum connections to be available
			await electrum.ready();

			// Broadcast the raw transaction
			const txid = await electrum.request('blockchain.transaction.broadcast', rawTransactionHex);

			return txid;
		}
		catch(error)
		{
			// Log an error message.
			debug.errors('Failed to broadcast transaction: ', error);
			debug.errors(rawTransactionHex);

			// Throw the error.
			throw(error);
		}
	}

	/**
	 * Wrapper that finds UTXOs for a contract
	 *
	 * @param contractInstance   fully prepared contract instance to call findCoins() on.
	 *
	 * @returns the list of UTXOs in the contract instance's control
	 *
	 * @ignore
	 */
	async findContractCoins(contractInstance: Instance): Promise<Utxo[]>
	{
		const scripthash = addressToElectrumScriptHash(contractInstance.address);

		// Wait for electrum connections to be available
		await electrum.ready();

		// Retrieve the UTXOs
		const result = await electrum.request('blockchain.scripthash.listunspent', scripthash);

		// Map the results to the BITBOX format for use in CashScript
		const utxos = result.map((utxo: any) => ({
			txid: utxo.tx_hash,
			vout: utxo.tx_pos,
			amount: utxo.value / SATS_PER_BCH,
			satoshis: utxo.value,
			// Confirmations is actually not the same as utxo.height, but it does not
			// get used by CashScript so it is fine
			confirmations: utxo.height,
		}));

		// Return a list of all UTXOs at the contract address.
		return utxos;
	}

	/**
	 * Simulates contract outcome based on contract parameters, total satoshis in the contract and the redemption price.
	 *
	 * @param contractParameters    contract parameters including price boundaries and truncation information.
	 * @param totalSats             total number of satoshis to simulate distribution of.
	 * @param redeemPrice           price (units/BCH) to base the redemption simulation on.
	 *
	 * @returns the TXID of a successful transaction.
	 *
	 * @ignore
	 */
	async simulate(
		contractParameters: ContractParameters,
		totalSats: number,
		redeemPrice: number,
	): Promise<SimulationOutput>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'simulate() <=', arguments ]);

		// Throw an error if provided parameters are not integers
		if(!isInt(totalSats) || !isInt(redeemPrice))
		{
			throw(new Error('Provided parameters must be integers'));
		}

		// Store truncation level information in local variables for brevity
		const highLowDelta = contractParameters.highLowDeltaTruncatedZeroes.byteLength;
		const lowTruncSize = contractParameters.lowTruncatedZeroes.byteLength;

		// calculate the clamped price for the contract outcomes.
		const clampedPrice = Math.max(Math.min(redeemPrice, contractParameters.highLiquidationPrice), contractParameters.lowLiquidationPrice);

		// Divide the untruncated hedge payout sats with the clamped price
		// and untruncate it to the low truncation level.
		const hedgeDivHighTrunc = Math.floor(contractParameters.hedgeUnitsXSatsPerBchHighTrunc / clampedPrice);
		const hedgeDivLowTrunc = untruncScriptNum(hedgeDivHighTrunc, highLowDelta);

		// Mod the untruncated hedge payout sats with the clamped price.
		const hedgeModHighTrunc = contractParameters.hedgeUnitsXSatsPerBchHighTrunc % clampedPrice;

		// Calculate mod extension size and apply mod extension.
		const modExtensionSize = Math.min(4 - scriptNumSize(hedgeModHighTrunc), highLowDelta);
		const hedgeModExt = untruncScriptNum(hedgeModHighTrunc, modExtensionSize);

		// Calculate price truncation size and apply price truncation.
		const priceTruncSize = highLowDelta - modExtensionSize;
		const truncatedPrice = truncScriptNum(clampedPrice, priceTruncSize);

		// Throw an error on division by zero
		if(truncatedPrice === 0)
		{
			throw(new Error('This configuration results in a division by zero'));
		}

		// Calculate the hedge LowTrunc payout from hedge DIV and MOD parts.
		const hedgeSatsLowTrunc = Math.min(hedgeDivLowTrunc + Math.floor(hedgeModExt / truncatedPrice), contractParameters.payoutSatsLowTrunc);
		// Calculate the long LowTrunc payout.
		const longSatsLowTrunc = contractParameters.payoutSatsLowTrunc - hedgeSatsLowTrunc;

		// Untruncate both payout values and add the dust protection.
		const hedgePayoutSats = dustsafe(untruncScriptNum(hedgeSatsLowTrunc, lowTruncSize));
		const longPayoutSats = dustsafe(untruncScriptNum(longSatsLowTrunc, lowTruncSize));

		// Calculate the total payout sats and consider the remainder to be miner fees.
		const payoutSats = hedgePayoutSats + longPayoutSats;
		const minerFeeSats = totalSats - payoutSats;

		const result = { hedgePayoutSats, longPayoutSats, payoutSats, minerFeeSats };

		// Write log entry for easier debugging.
		debug.action('Simulating contract outcomes.');

		// Output function result for easier collection of test data.
		debug.result([ 'simulate() =>', result ]);

		// Return the results of the calculation.
		return result;
	}

	/**
	 * Builds a contract instance from contract parameters.
	 *
	 * @param contractHashes   contract hashes required to build the contract instance.
	 *
	 * @returns a contract instance.
	 */
	async build(contractHashes: ContractHashes): Promise<Instance>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'build() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Creating contract instance.');

		// Create an instance of the contract using the parameters above.
		const contract = this.contractFactory.new(
			contractHashes.mutualRedemptionDataHash,
			contractHashes.payoutDataHash,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'build() =>', contract ]);

		// Pass back the contract to the caller.
		return contract;
	}

	/**
	 * Creates a new contract.
	 *
	 * @param oraclePublicKeyBuffer             public key for the oracle that the contract trusts for price messages.
	 * @param hedgePublicKeyBuffer              public key for the hedge party.
	 * @param longPublicKeyBuffer               public key for the long party.
	 * @param hedgeUnits                        amount in units that the hedge party wants to protect against volatility.
	 * @param startPrice                        starting price (units/BCH) of the contract.
	 * @param startBlockHeight                  blockHeight at which the contract is considered to have been started at.
	 * @param earliestLiquidationModifier       minimum number of blocks from the starting height before the contract can be liquidated.
	 * @param maturityModifier                  exact number of blocks from the starting height that the contract should mature at.
	 * @param highLiquidationPriceMultiplier    multiplier for the startPrice determining the upper liquidation price boundary.
	 * @param lowLiquidationPriceMultiplier     multiplier for the startPrice determining the lower liquidation price boundary.
	 *
	 * @returns the contract parameters, metadata and hashes.
	 */
	async create(
		oraclePublicKeyBuffer: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		hedgeUnits: number,
		startPrice: number,
		startBlockHeight: number,
		earliestLiquidationModifier: number,
		maturityModifier: number,
		highLiquidationPriceMultiplier: number,
		lowLiquidationPriceMultiplier: number,
	): Promise<ContractData>
	{
		// If the hedge units are larger than MAX_HEDGE_UNITS, the contract is
		// at risk of being unredeemable with certain configurations and prices
		// which is why we disallow it in the safe create() function (#84).
		if(hedgeUnits > MAX_HEDGE_UNITS)
		{
			throw(new Error(`Hedge units (${hedgeUnits}) cannot be > ${MAX_HEDGE_UNITS}. `
				+ 'The current value might result in unredeemable contracts.'));
		}

		// If the hedge units are smaller than MIN_HEDGE_UNITS, the contract runs
		// into precision errors according to Karol's math analysis (#93).
		if(hedgeUnits < MIN_HEDGE_UNITS)
		{
			throw(new Error(`Hedge units (${hedgeUnits}) cannot be < ${MIN_HEDGE_UNITS}. `
				+ 'The current value might result in a high level of imprecision in calculations.'));
		}

		// If the start price is too high or too low, the contract runs into precision errors (#93).
		if((startPrice < MIN_START_PRICE) || (MAX_START_PRICE < startPrice))
		{
			throw(new Error(`Start price (${startPrice}) must be in the inclusive range [${MIN_START_PRICE}, ${MAX_START_PRICE}]. `
				+ 'The current value might result in a high level of imprecision in calculations. '
				+ 'An alternative is to use an oracle and related calculations that scale the price, for example JPY x 10^-3 (milli-JPY) instead of plain JPY.'));
		}

		const contractData = await this.createUnsafe(
			oraclePublicKeyBuffer,
			hedgePublicKeyBuffer,
			longPublicKeyBuffer,
			hedgeUnits,
			startPrice,
			startBlockHeight,
			earliestLiquidationModifier,
			maturityModifier,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
		);

		// Check that delta is not larger than 2 (should not happen because of the hedge units check) (#84)
		const truncationSizeDelta = contractData.parameters.highLowDeltaTruncatedZeroes.byteLength;
		const maxDelta = 2;
		if(truncationSizeDelta > maxDelta)
		{
			throw(new Error(`Truncation delta amount (${truncationSizeDelta}) cannot be > ${maxDelta}). `
				+ 'The current value might result in a high level of imprecision in calculations. '
				+ 'The current value may cause the contract to be unredeemable. '
				+ `Reducing hedge units (${hedgeUnits}) or reducing start price (${startPrice}) will have the most impact on this value. `
				+ 'An alternative is to use an oracle and related calculations that scale the price, for example JPY x 10^-3 (milli-JPY) instead of plain JPY.'));
		}

		// Disallow low truncation levels higher than 3, since the only way these can work
		// is if more than 21M BCH exist.
		const truncationSizeLow = contractData.parameters.lowTruncatedZeroes.byteLength;
		const maxTruncationSizeLow = 3;
		if(truncationSizeLow > maxTruncationSizeLow)
		{
			throw(new Error(`Low truncation amount (${truncationSizeLow}) cannot be > ${maxTruncationSizeLow}). `
				+ 'The current value may cause the contract to be unredeemable. '
				+ `Reducing hedge units (${hedgeUnits}) or increasing start price (${startPrice}) will have the most impact on this value. `
				+ 'An alternative is to use an oracle and related calculations that scale the price, for example JPY x 10^-3 (milli-JPY) instead of plain JPY.'));
		}

		// If the liquidation range is too large on either side, we run into precision errors (#93)
		const { lowLiquidationPrice, highLiquidationPrice } = contractData.parameters;
		if(highLiquidationPrice > MAX_HIGH_LIQUIDATION_PRICE)
		{
			throw(new Error(`High liquidation price (${highLiquidationPrice}) cannot be > ${MAX_HIGH_LIQUIDATION_PRICE}. `
				+ 'The current value might result in a high level of imprecision in calculations. '
				+ `Reducing start price ${startPrice} might fix this.`
				+ `Reducing high liquidation price multiplier ${highLiquidationPriceMultiplier} might fix this.`
				+ 'An alternative is to use an oracle and related calculations that scale the price, for example JPY x 10^+3 (mega-JPY) instead of plain JPY.'));
		}
		if(lowLiquidationPrice < MIN_LOW_LIQUIDATION_PRICE)
		{
			throw(new Error(`Low liquidation price (${lowLiquidationPrice}) cannot be < ${MIN_LOW_LIQUIDATION_PRICE}. `
				+ 'The current value might result in a high level of imprecision in calculations. '
				+ `Increasing start price ${startPrice} might fix this.`
				+ `Increasing low liquidation price multiplier ${lowLiquidationPriceMultiplier} might fix this.`
				+ 'An alternative is to use an oracle and related calculations that scale the price, for example JPY x 10^-3 (milli-JPY) instead of plain JPY.'));
		}

		// If the liquidation range is inverted or equal to start price, we get immediate liquidation
		if(highLiquidationPrice <= startPrice)
		{
			throw(new Error(`High liquidation price (${highLiquidationPrice}) cannot be <= start price (${startPrice}). `
				+ 'The current value will cause immediate liquidation. '
				+ 'Making high liquidation price multiplier sufficiently > 1 will fix this.'));
		}
		if(lowLiquidationPrice >= startPrice)
		{
			throw(new Error(`Low liquidation price (${lowLiquidationPrice}) cannot be >= start price (${startPrice}). `
				+ 'The current value will cause immediate liquidation. '
				+ 'Making low liquidation price multiplier sufficiently < 1 will fix this.'));
		}

		return contractData;
	}

	/**
	 * Creates a new contract without extra safety checks.
	 *
	 * @param oraclePublicKeyBuffer             public key for the oracle that the contract trusts for price messages.
	 * @param hedgePublicKeyBuffer              public key for the hedge party.
	 * @param longPublicKeyBuffer               public key for the long party.
	 * @param hedgeUnits                        amount in units that the hedge party wants to protect against volatility.
	 * @param startPrice                        starting price (units/BCH) of the contract.
	 * @param startBlockHeight                  blockHeight at which the contract is considered to have been started at.
	 * @param earliestLiquidationModifier       minimum number of blocks from the starting height before the contract can be liquidated.
	 * @param maturityModifier                  exact number of blocks from the starting height that the contract should mature at.
	 * @param highLiquidationPriceMultiplier    multiplier for the startPrice determining the upper liquidation price boundary.
	 * @param lowLiquidationPriceMultiplier     multiplier for the startPrice determining the lower liquidation price boundary.
	 *
	 * @returns the contract parameters, metadata and hashes.
	 *
	 * @ignore
	 */
	async createUnsafe(
		oraclePublicKeyBuffer: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		hedgeUnits: number,
		startPrice: number,
		startBlockHeight: number,
		earliestLiquidationModifier: number,
		maturityModifier: number,
		highLiquidationPriceMultiplier: number,
		lowLiquidationPriceMultiplier: number,
	): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'createUnsafe() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Preparing a new contract.');

		// There are 4 root numbers from which other values are derived.
		// 1. Low liquidation price.
		// 2. High liquidation price
		// 3. Hedge input satoshis.
		// 4. Composite value representing Hedge's unit value.

		// 1. Low liquidation price: the low price that triggers liquidation.
		// The value is rounded to achieve a result as close as possible to intent.
		// More strict terms such as floor and ceiling are imposed on derivative values.
		const lowLiquidationPrice = Math.round(lowLiquidationPriceMultiplier * startPrice);

		// Low liquidation price <= zero may cause the contract to be unredeemable
		if(lowLiquidationPrice <= 0)
		{
			throw(new Error(`Low liquidation price (${lowLiquidationPrice}) cannot be <= 0. `
				+ 'The current value may cause the contract to be unredeemable. '
				+ 'Making low liquidation price multiplier sufficiently > 0 will fix this.'));
		}

		// 2. High liquidation price: the high price that triggers liquidation.
		// The value is rounded to achieve a result as close as possible to intent.
		const highLiquidationPrice = Math.round(highLiquidationPriceMultiplier * startPrice);

		// High liquidation price greater than SCRIPT_INT_MAX causes the contract to be unredeemable.
		if(highLiquidationPrice > SCRIPT_INT_MAX)
		{
			throw(new Error(`High liquidation price (${highLiquidationPrice}) cannot be > maximum script integer (${SCRIPT_INT_MAX}). `
				+ 'The current value may cause the contract to be unredeemable. '
				+ 'Making high liquidation price multiplier sufficiently closer to 1 will fix this.'));
		}

		// High liquidation has a constraint regarding payout in extreme cases where only a small amount of satoshis
		// are needed to payout Hedge. The most extreme case is 1 satoshi. To retain a reasonable step-precision
		// between values, and also to match the reality of dust requirements, we choose the dust limit as the
		// minimum hedge payout. The result of the calculation is that for a requested set of parameters, there is
		// a minimum Hedge units that will satisfy the extreme end of high liquidation conditions.
		// One method of calculation, focused on the input modifier is as follows:
		//   hedgePayoutSats @ highLiquidationPrice >= MIN_HEDGE_PAYOUT_SATS
		//   hedgeUnits * SATS_PER_BCH / highLiquidationPrice >= MIN_HEDGE_PAYOUT_SATS
		//   hedgeUnits >= MIN_HEDGE_PAYOUT_SATS * highLiquidationPrice / SATS_PER_BCH
		//     and for MIN_HEDGE_PAYOUT_SATS = DUST:
		//   hedgeUnits >= DUST * highLiquidationPrice / SATS_PER_BCH
		const dynamicMinHedgeUnits = (DUST_LIMIT * highLiquidationPrice) / SATS_PER_BCH;
		if(hedgeUnits < dynamicMinHedgeUnits)
		{
			// Recalculating around the price multiplier to provide an actionable error message:
			//   highLiquidationPrice <= hedgeUnits * SATS_PER_BCH / DUST
			//     substituting with the high liquidation modifier:
			//   highLiquidationPriceMultiplier * startPrice <= hedgeUnits * SATS_PER_BCH / DUST
			//   highLiquidationPriceMultiplier <= (hedgeUnits * SATS_PER_BCH) / (DUST * startPrice)
			const maxHighLiquidationPriceMultiplier = (hedgeUnits * SATS_PER_BCH) / (DUST_LIMIT * startPrice);
			throw(new Error(`Hedge units (${hedgeUnits}) cannot be < ${dynamicMinHedgeUnits} and high liquidation price multiplier (${highLiquidationPriceMultiplier}) cannot be > ${maxHighLiquidationPriceMultiplier}. `
				+ 'The current values may cause the contract to be unredeemable at high prices.'));
		}

		// 3. Hedge input satoshis.
		// Hedge: Satoshis equal to the hedged unit value at the start price.
		//        The value is rounded to achieve a result as close as possible to intent.
		//        More strict terms such as floor and ceiling are imposed on derivative values.
		// For readability, we also derive the naive values of total and long satoshis which will be adjusted later.
		// Total: Satoshis equal to the hedged unit value at the low liquidation price. I.e. long gets about zero.
		//        The value is ceiling to ensure that the result is *at least* enough to cover hedge value, never less.
		// Long:  Satoshis equal to difference between the total satoshis and hedge satoshis.
		//        The value is recorded for metadata purposes only.
		const hedgeInputSats = Math.round((hedgeUnits * SATS_PER_BCH) / startPrice);
		const naiveTotalInputSats = Math.ceil((hedgeUnits * SATS_PER_BCH) / lowLiquidationPrice);
		const naiveLongInputSats = naiveTotalInputSats - hedgeInputSats;

		// Regarding the truncation operations in the remaining code:
		// Due to current limitations of Bitcoin Cash script, calculations can only be performed with 32-bit numbers.
		// Combined with other current limitations, 32 bits is not enough to handle meaningful amounts of value
		// in AnyHedge. Therefore some mathematical tricks are required to get around the limitation. The trick that
		// AnyHedge currently uses is to truncate bytes from large values and remember how much has been removed
		// so it can be added back later.
		// Specifically, AnyHedge has two levels of truncation required to handle two specific large numbers:
		//   HighTrunc) Required for the contract to do calculations with the composite number calculated below in 4.
		//   LowTrunc)  Required for the contract to do calculations with the payoutSats calculated below.

		// 4. Composite number representing Hedge's unit value.
		// The number is calculated as hedge units * 1e8 sats/bch.
		// This overcomes the current limits of BCH scripting where we have division but no multiplication.
		// The value divided by the price in BCH directly yields satoshis for hedge value at said price.
		// The value is naive because it may require truncation for storage and calculations in the contract.
		// The value is rounded to achieve a result as close as possible to intent.
		// More strict terms such as floor and ceiling are imposed on derivative values.
		const naiveHedgeUnitsXSatsPerBch = Math.round(hedgeUnits * SATS_PER_BCH);

		// Calculate the required amount of truncation and record the truncated value.
		const truncationSizeHigh = calculateRequiredScriptNumTruncation(naiveHedgeUnitsXSatsPerBch);
		const hedgeUnitsXSatsPerBchHighTrunc = truncScriptNum(naiveHedgeUnitsXSatsPerBch, truncationSizeHigh);

		// After the 4 root values, we derive the remaining money-related numbers for the contract and metadata.

		// Total sats are truncated as described above if necessary for the final contract value.
		// Total input sats are renamed to payout sats to align with contract parameter names
		const truncationSizeLow = calculateRequiredScriptNumTruncation(naiveTotalInputSats);
		const payoutSatsLowTrunc = truncScriptNum(naiveTotalInputSats, truncationSizeLow);

		// The difference between the high and low truncation is needed in the contract so we calculate it here.
		const truncationSizeDelta = truncationSizeHigh - truncationSizeLow;

		// If the truncation delta is negative, the contract is always unredeemable
		if(truncationSizeDelta < 0)
		{
			throw(new Error(`Truncation delta amount (${truncationSizeDelta}) cannot be < 0). `
				+ 'The current value may cause the contract to be unredeemable. '
				+ 'There may be a fundamental problem with the input parameters.'));
		}

		// Total sats, long input sats, long input units are calculated only for metadata
		const totalInputSats = untruncScriptNum(payoutSatsLowTrunc, truncationSizeLow);
		const longInputSats = totalInputSats - hedgeInputSats;
		const longInputUnits = ((longInputSats / SATS_PER_BCH) * startPrice);

		// In addition to money-related numbers, we derive time-related numbers for the contract and metadata.

		// Earliest liquidation provides a grace period in which the contract cannot settle.
		// Maturity provides a deadline after which anyone can settle the contract using the maturity block's first price message.
		const earliestLiquidationHeight = startBlockHeight + earliestLiquidationModifier;
		const maturityHeight = startBlockHeight + maturityModifier;

		// We also package keys and other fixed values for the contract and metadata.

		// Assemble the mutual redemption data.
		const hedgeMutualRedeemPubk = hedgePublicKeyBuffer;
		const longMutualRedeemPubk = longPublicKeyBuffer;
		const mutualRedemptionData =
		[
			hedgeMutualRedeemPubk,
			longMutualRedeemPubk,
		];

		// Create hedge and long lock scripts.
		const hedgeLockScript = buildLockScriptP2PKH(hedgePublicKeyBuffer);
		const longLockScript = buildLockScriptP2PKH(longPublicKeyBuffer);

		// Copy the public key from the oracles wallet.
		const oraclePubk = oraclePublicKeyBuffer;

		// Create buffers with zeroes matching the low and delta truncation sizes.
		const lowTruncatedZeroes = Buffer.alloc(truncationSizeLow);
		const highLowDeltaTruncatedZeroes = Buffer.alloc(truncationSizeDelta);

		// Create buffers for integer parameters for consistent assembly and hashing.
		const payoutSatsLowTruncBuffer =             bitbox.Script.encodeNumber(payoutSatsLowTrunc);
		const hedgeUnitsXSatsPerBchHighTruncBuffer = bitbox.Script.encodeNumber(hedgeUnitsXSatsPerBchHighTrunc);
		const lowLiquidationPriceBuffer =            bitbox.Script.encodeNumber(lowLiquidationPrice);
		const highLiquidationPriceBuffer =           bitbox.Script.encodeNumber(highLiquidationPrice);
		const earliestLiquidationHeightBuffer =      bitbox.Script.encodeNumber(earliestLiquidationHeight);
		const maturityHeightBuffer =                 bitbox.Script.encodeNumber(maturityHeight);

		// Assemble the payout data.
		const payoutData =
		[
			hedgeLockScript,
			longLockScript,
			oraclePubk,
			hedgeUnitsXSatsPerBchHighTruncBuffer,
			highLowDeltaTruncatedZeroes,
			payoutSatsLowTruncBuffer,
			lowTruncatedZeroes,
			lowLiquidationPriceBuffer,
			highLiquidationPriceBuffer,
			earliestLiquidationHeightBuffer,
			maturityHeightBuffer,
		];

		// Calculate the hashes required to create a contract instance.
		const mutualRedemptionDataHash = bitbox.Crypto.ripemd160(bitbox.Crypto.sha256(Buffer.concat(mutualRedemptionData)));
		const payoutDataHash = bitbox.Crypto.ripemd160(bitbox.Crypto.sha256(Buffer.concat(payoutData)));

		// Store the public keys.
		const oraclePublicKey = oraclePublicKeyBuffer.toString('hex');
		const hedgePublicKey = hedgePublicKeyBuffer.toString('hex');
		const longPublicKey = longPublicKeyBuffer.toString('hex');

		// Store the total dust protection cost.
		const dustCost = 2 * DUST_LIMIT;

		// Store the miner transaction cost.
		const minerCost = MIN_TRANSACTION_FEE;

		// Assemble the contract metadata.
		const contractMetadata =
		{
			oraclePublicKey,
			hedgePublicKey,
			longPublicKey,
			startBlockHeight,
			maturityModifier,
			earliestLiquidationModifier,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
			startPrice,
			hedgeUnits,
			longInputUnits,
			totalInputSats,
			hedgeInputSats,
			longInputSats,
			dustCost,
			minerCost,
		};

		// Assemble the contract parameters.
		const contractParameters =
		{
			lowLiquidationPrice,
			highLiquidationPrice,
			earliestLiquidationHeight,
			maturityHeight,
			oraclePubk,
			hedgeLockScript,
			longLockScript,
			hedgeMutualRedeemPubk,
			longMutualRedeemPubk,
			lowTruncatedZeroes,
			highLowDeltaTruncatedZeroes,
			hedgeUnitsXSatsPerBchHighTrunc,
			payoutSatsLowTrunc,
		};

		// Assemble the contract hashes.
		const contractHashes =
		{
			mutualRedemptionDataHash,
			payoutDataHash,
		};

		// Assemble the final contract data.
		const contractData =
		{
			parameters: contractParameters,
			metadata: contractMetadata,
			hashes: contractHashes,
		};

		// Output function result for easier collection of test data.
		debug.result([ 'createUnsafe() =>', contractData ]);

		// Pass back the contract data to the caller.
		return contractData;
	}
}

export = AnyHedgeManager;
