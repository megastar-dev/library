import { Crypto } from 'bitbox-sdk';
import { hexToBin, cashAddressToLockingBytecode } from '@bitauth/libauth';

/**
* Helper function to construct a P2PKH locking script Buffer from a public key
*
* @param publicKey   Public key to create a P2PKH locking script for
*
* @returns a P2PKH locking script corresponding to the passed public key
*/
export const buildLockScriptP2PKH = function(publicKey: Buffer): Buffer
{
	const publicKeyHash = (new Crypto()).hash160(publicKey);

	// Build P2PKH lock script (PUSH<25> DUP HASH160 PUSH<20> <public key hash> EQUALVERIFY CHECKSIG)
	return Buffer.concat([ hexToBin('1976a914'), publicKeyHash, hexToBin('88ac') ]);
};

/**
* Helper function to construct a P2SH locking script Buffer from a script bytecode
*
* @param scriptBytecode   Bytecode of the script for which to create a P2SH locking script
*
* @returns a P2SH locking script corresponding to the passed script bytecode
*/
export const buildLockScriptP2SH = function(scriptBytecode: Buffer): Buffer
{
	const scriptHash = (new Crypto()).hash160(scriptBytecode);

	// Build P2SH lock script (PUSH<23> HASH160 PUSH<20> <script hash> EQUAL)
	return Buffer.concat([ hexToBin('17a914'), scriptHash, hexToBin('87') ]);
};

/**
* Helper function to convert an address to a locking script
*
* @param address   Address to convert to locking script
*
* @returns a locking script corresponding to the passed address
*/
export const addressToLockScript = function(address: string): Uint8Array
{
	const result = cashAddressToLockingBytecode(address);

	// The `cashAddressToLockingBytecode()` call returns an error string OR the correct bytecode
	// so we check if it errors, in which case we throw the error, otherwise return the result
	if(typeof result === 'string')
	{
		throw(new Error(result));
	}

	return result.bytecode;
};

/**
 * Helper function to convert an address to an electrum-cash compatible scripthash.
 * This is necessary to support electrum versions lower than 1.4.3, which do not
 * support addresses, only script hashes.
 *
 * @param address Address to convert to an electrum scripthash
 *
 * @returns The corresponding script hash in an electrum-cash compatible format
 */
export const addressToElectrumScriptHash = function(address: string): string
{
	// Retrieve locking script
	const lockScript = addressToLockScript(address);

	// Hash locking script
	const scriptHash = (new Crypto()).sha256(Buffer.from(lockScript));

	// Reverse scripthash
	scriptHash.reverse();

	// Return scripthash as a hex string
	return scriptHash.toString('hex');
};
