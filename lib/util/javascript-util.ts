import { debug as debugLogger, Debugger } from 'debug';

// Initialize support for debug message management.
// Note: This interface needs to be added for this project to be included in a
// JavaScript project as a git-based dependency. When we publish to NPM, this
// can be removed.
export interface DebugType
{
	action: Debugger;
	object: Debugger;
	errors: Debugger;
	params: Debugger;
	result: Debugger;
}
export const debug: DebugType =
{
	action:	debugLogger('anyhedge:action'),
	object:	debugLogger('anyhedge:object'),
	errors:	debugLogger('anyhedge:errors'),
	params:	debugLogger('anyhedge:params'),
	result:	debugLogger('anyhedge:result'),
};

/**
* Utility function that creates a range of numbers
*
* @param stop   The first number outside of the range
*
* @returns an array with integers from 0 to `stop` (excluding `stop`)
*/
export const range = (stop: number): number[] => [ ...Array(stop).keys() ];

/**
* Utility function that can be used to restore Buffers from JSON.
*
* @param k   key part of a key-value pair.
* @param v   value part of a key-value pair.
*
* @returns a Buffer restored from the `v.data` field, or the `v` value.
*/
export const bufferReviver = function(k: any, v: any): Buffer | any
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'bufferReviver() <=', arguments ]);

	// Check if the data might be a buffer..
	const condition =
	(
		v !== null
		&& typeof v === 'object'
		&& 'type' in v
		&& v.type === 'Buffer'
		&& 'data' in v
		&& Array.isArray(v.data)
	);

	// Determine what to return based on condition.
	const result = (condition ? Buffer.from(v.data) : v);

	// Output function result for easier collection of test data.
	debug.result([ 'bufferReviver() =>', result ]);

	// Return the parsed content.
	return result;
};

/**
* Utility function that checks whether a passed JS number is an integer
*
* @param num   The number to check
*
* @returns a boolean indication the integer-ness of the number
*/
export const isInt = (num: number): boolean => (num % 1 === 0);
