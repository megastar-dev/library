const ElectrumCluster = require('electrum-cash').Cluster;

const electrum = new ElectrumCluster('Electrum cluster example', '1.4.1', 2, 3, ElectrumCluster.ORDER.RANDOM);

electrum.addServer('bch.imaginary.cash');
electrum.addServer('electroncash.de');
electrum.addServer('electroncash.dk');
electrum.addServer('bitcoincash.network');
electrum.addServer('fulcrum.fountainhead.cash');

export default electrum;
